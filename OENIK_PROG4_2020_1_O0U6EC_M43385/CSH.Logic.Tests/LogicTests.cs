﻿using CSH.GameModel;
using CSH.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Logic.Tests
{
    [TestFixture]
    public class LogicTests
    {
        [Test]
        public void MainMenu_ClickWorks()
        {
            // ARRANGE
            Mock<IGameData> gData = new Mock<IGameData>();
            MainMenuLogic mainLogic = new MainMenuLogic(gData.Object);
            gData.Setup(x => x.GetGuiElements(0)).Returns(new List<GUIElement>()
            {
                new GUIElement(0, 640, 360, 1280, 720, 8, string.Empty), // Háttérkép
                new GUIElement(1, 640, 350, 575, 75, 10, string.Empty),
                new GUIElement(2, 640, 530, 575, 75, 6, string.Empty),
                new GUIElement(3, 640, 620, 575, 75, 2, string.Empty)
            });

            // ACT
            int menuMode = 0;

            mainLogic.MouseClick(640, 530, ref menuMode);
            int expected = 2;

            // ASSERT
            Assert.That(menuMode == expected);
        }

        [Test]
        public void NewGameMenu_WritingLetterWorks()
        {
            // ARRANGE
            Mock<IGameData> gData = new Mock<IGameData>();
            Mock<IModel> gModel = new Mock<IModel>();
            Mock<IRepository> gRepo = new Mock<IRepository>();
            gData.Setup(x => x.GetGuiElements(1)).Returns(new List<GUIElement>()
            {
                new GUIElement(0, 640, 360, 1280, 720, 9, string.Empty),
                new GUIElement(0, 500, 540, 80, 50, 1, "NAME:"),
                new GUIElement(1, 120, 630, 100, 50, 0, string.Empty),
                new GUIElement(2, 640, 630, 150, 50, 10, string.Empty),
            });

            NewGameMenuLogic newLogic = new NewGameMenuLogic(gData.Object, gModel.Object, gRepo.Object);

            // ACT
            newLogic.KeyDown("O");
            newLogic.KeyDown("E");
            newLogic.KeyDown("N");
            newLogic.KeyDown("I");
            newLogic.KeyDown("K");

            List<IRenderable> l = newLogic.GetObjects().ToList();

            // ASSERT
            Assert.That(l[4].Text.Equals("OENIK"));
        }

        [Test]
        public void HighscoreMenu_CorrectObjectNumbers()
        {
            // ARRANGE
            Mock<IGameData> gData = new Mock<IGameData>();
            Mock<IRepository> gRepo = new Mock<IRepository>();
            gData.Setup(x => x.GetGuiElements(2)).Returns(new List<GUIElement>()
            {
                new GUIElement(3, 640, 360, 1280, 720, 5, string.Empty),
                new GUIElement(0, 120, 630, 100, 50, 0, string.Empty)
            });

            HighscoreMenuLogic highLogic = new HighscoreMenuLogic(gData.Object, gRepo.Object);

            // ACT


            List<IRenderable> list = highLogic.GetObjects().ToList();

            // ASSERT
            Assert.That(list.Count == 2);
        }

        [Test]
        public void GUIElementTypeIsCorrect()
        {
            // ARRANGE
            Mock<IGameData> gData = new Mock<IGameData>();
            Mock<IRepository> gRepo = new Mock<IRepository>();

            gData.Setup(x => x.GetGuiElements(2)).Returns(new List<GUIElement>()
            {
                new GUIElement(0, 640, 360, 1280, 720, 9, string.Empty),
                new GUIElement(0, 500, 540, 80, 50, 1, "NAME:"),
                new GUIElement(1, 120, 630, 100, 50, 0, string.Empty),
                new GUIElement(2, 640, 630, 150, 50, 10, string.Empty),
            });
            
            HighscoreMenuLogic hLogic = new HighscoreMenuLogic(gData.Object, gRepo.Object);

            // ACT
            hLogic.Update();
            IEnumerable<IRenderable> list = hLogic.GetObjects();

            // ASSERT
            Assert.That(list, Is.TypeOf(typeof(List<GameObject>)));
        }

        [TestCase(120, 630, 0)]
        [TestCase(640, 630, 1)]
        [TestCase(200, 200, 1)]
        public void NewGameMenu_DeleteWorks(int x, int y, int expected)
        {
            // ARRANGE
            Mock<IGameData> gData = new Mock<IGameData>();
            Mock<IModel> gModel = new Mock<IModel>();
            Mock<IRepository> gRepo = new Mock<IRepository>();
            gData.Setup(k => k.GetGuiElements(1)).Returns(new List<GUIElement>()
            {
                new GUIElement(1, 120, 630, 100, 50, 0, string.Empty), //backbutton
                new GUIElement(2, 640, 630, 150, 50, 13, string.Empty), //playbutton
            });

            NewGameMenuLogic newLogic = new NewGameMenuLogic(gData.Object, gModel.Object, gRepo.Object);

            // ACT
            int menuMode = 1;
            newLogic.KeyDown("O");
            newLogic.KeyDown("E");
            newLogic.KeyDown("N");
            newLogic.KeyDown("I");
            newLogic.KeyDown("K");
            newLogic.KeyDown("Backspace");
            newLogic.KeyDown("Backspace");
            newLogic.KeyDown("Backspace");
            newLogic.KeyDown("Backspace");
            newLogic.KeyDown("Backspace");

            newLogic.MouseClick(x, y, ref menuMode);

            // ASSERT
            Assert.That(menuMode == expected);
        }
    }
}
