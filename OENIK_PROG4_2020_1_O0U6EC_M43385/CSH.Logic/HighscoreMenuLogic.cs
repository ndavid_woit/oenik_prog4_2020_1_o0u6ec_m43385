﻿using CSH.GameModel;
using CSH.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Logic
{
    public class HighscoreMenuLogic
    {
        private IGameData gameData;
        private IRepository repo;
        private IEnumerable<Record> highScoreInfos;
        private List<GUIElement> highScoreGuiElements;

        private bool isLoaded;

        public HighscoreMenuLogic(IGameData gameData, IRepository repo)
        {
            this.gameData = gameData;
            this.repo = repo;
            this.highScoreGuiElements = new List<GUIElement>();

            this.isLoaded = false;
        }

        public void Update()
        {
            if (!this.isLoaded)
            {
                this.highScoreInfos = this.repo.LoadHighscores();
                this.highScoreGuiElements.Clear();
                if (this.highScoreInfos != null)
                {
                    this.highScoreInfos = this.highScoreInfos.OrderByDescending(x => x.Highscore);
                    foreach (Record rc in this.highScoreInfos)
                    {
                        int index = this.highScoreGuiElements.Count;
                        string text = "NAME: " + rc.PlayerName
                            + "    DIFFICULTY: " + rc.Difficulty
                            + "    SCORE: " + rc.Highscore
                            + "    MAX STREAK: " + rc.MaxStreak;
                        this.highScoreGuiElements.Add(new GUIElement(0, 640, 200 + (index * 80), 700, 50, 1, text));
                    }
                }

                this.isLoaded = true;
            }
        }

        public void MouseClick(double x, double y, ref int menuMode)
        {
            foreach (GUIElement item in this.gameData.GetGuiElements(2))
            {
                if (item.Collide(x, y, 0, 0))
                {
                    switch (item.Id)
                    {
                        case 0: menuMode = 0; this.isLoaded = false; break;
                    }
                }
            }
        }

        public IEnumerable<IRenderable> GetObjects()
        {
            List<GameObject> list = new List<GameObject>();
            foreach (GUIElement item in this.gameData.GetGuiElements(2))
            {
                list.Add(item);
            }

            foreach (GUIElement item in this.highScoreGuiElements)
            {
                list.Add(item);
            }

            return list;
        }
    }
}
