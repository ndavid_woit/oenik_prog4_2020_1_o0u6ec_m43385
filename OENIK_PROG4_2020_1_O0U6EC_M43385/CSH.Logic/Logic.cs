﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSH.GameModel;
using CSH.Repository;
using System.Windows.Media;

namespace CSH.Logic
{
    /*  
        bedob a main manübe --> mainmenulogic.cs --> 3 lehetőség: játék, highscore, kilépés
            játék --> newgamemenulogic.cs --> bekéri a nevedet --> nehézségválasztás --> logic.cs --> highscoremenulogic.cs
            highscore --> highscoremenulogic.cs
            kilépés --> kilép
    */


    public class Logic : ILogic
    {
        int menuMode = 0; //0-mainmenu, 1-newgame, 2-highscore, 3-play

        IGameData gdata;
        public IModel model;
        IRepository repo;

        MainMenuLogic mL;
        NewGameMenuLogic nL; //ez lesz a loadgamelogic is
        HighscoreMenuLogic hL;
        GameLogic gL;


        public Logic(IGameData gd, IModel m, IRepository r)
        {
            this.gdata = gd;
            this.model = m;
            this.repo = r;

            mL = new MainMenuLogic(gd);
            nL = new NewGameMenuLogic(gd, m, r); //ez lesz a loadgamelogic is
            hL = new HighscoreMenuLogic(gd, repo);
            gL = new GameLogic(m, gd, r);

            this.menuMode = 0;
        }


        public void MouseClick(double x, double y)
        {
            switch (this.menuMode)
            {
                case 0: this.mL.MouseClick(x, y, ref this.menuMode); break;
                case 1: this.nL.MouseClick(x, y, ref this.menuMode); break;
                case 2: this.hL.MouseClick(x, y, ref this.menuMode); break;
                case 3: this.gL.MouseClick(x, y, ref this.menuMode); break;
            }
        }

        public void KeyDown(string keyName)
        {
            switch (this.menuMode)
            {
                case 1: this.nL.KeyDown(keyName); break;
                case 3: this.gL.KeyDown(keyName); break;
            }
        }

        public void Update()
        {
            switch (this.menuMode)
            {
                case 2: hL.Update(); break;
                case 3: gL.Update(); break;
            }
        }

        public IEnumerable<IRenderable> GetObjects()
        {
            switch (this.menuMode)
            {
                case 0: return this.mL.GetObjects();
                case 1: return this.nL.GetObjects();
                case 2: return this.hL.GetObjects();
                case 3: return this.gL.GetObjects();
            }
            return null;
        }

        public void CallForSave()
        {
            repo.SaveHighscore(new Record(model.PlayerName,model.Score,model.Streak,model.Difficulty));
        }


    }
}
