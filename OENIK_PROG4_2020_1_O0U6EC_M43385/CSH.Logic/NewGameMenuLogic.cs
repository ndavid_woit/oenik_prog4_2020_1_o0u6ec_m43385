﻿using CSH.GameModel;
using CSH.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;

namespace CSH.Logic
{
    public class NewGameMenuLogic
    {
        private IGameData gameData;
        private IModel gameModel;

        public IRepository repo;


        private GameObject playerName;
        private GameObject selectedDiff;
        private GameObject selectedMusic;

        private int difficulty;
        private int musicID;
        private List<Record> highscores;
        public NewGameMenuLogic(IGameData gameData, IModel gameModel, IRepository repo)
        {
            this.repo = repo;

            this.gameData = gameData;
            this.gameModel = gameModel;

            this.playerName = new GameObject(640, 540, 200, 50, 1) { Text = string.Empty };
            this.selectedDiff = new GameObject(640, 410, 160, 90, 30);
            this.selectedMusic = new GameObject(640, 250, 160, 140, 30);
            this.difficulty = 1;
            this.musicID = 1;
            this.highscores = repo.LoadHighscores();
        }

        public void MouseClick(double x, double y, ref int menuMode)
        {
            foreach (GUIElement item in this.gameData.GetGuiElements(1))
            {
                if (item.Collide(x, y, 0, 0))
                {
                    switch (item.Id)
                    {
                        case 1: menuMode = 0; break;
                        case 2:
                            if (!this.playerName.Text.Equals(string.Empty))
                            {
                                if (highscores != null && highscores.Select(r => r.PlayerName).Contains(this.playerName.Text)) // Itt történik a betöltés
                                {
                                    menuMode = 3;
                                    int score = highscores.Where(r => r.PlayerName == this.playerName.Text).FirstOrDefault().Highscore;
                                    this.musicID = gameData.GetMusic(this.difficulty).Id;
                                    this.gameModel.LoadNewGame(this.gameData, this.playerName.Text, this.musicID, this.difficulty, score);
                                }
                                else
                                {
                                    menuMode = 3;
                                    this.musicID = gameData.GetMusic(this.difficulty).Id;
                                    this.gameModel.LoadNewGame(this.gameData, this.playerName.Text, this.musicID, this.difficulty, 0);
                                }
                                
                            }

                            break;
                        case 3: this.difficulty = 0; break;
                        case 4: this.difficulty = 1; break;
                        case 5: this.difficulty = 2; break;
                        // case 6: this.musicID = 0; break;
                        // case 7: this.musicID = 1; break;
                        // case 8: this.musicID = 2; break;
                    }

                    this.selectedDiff.X = 450 + (this.difficulty * 190);
                    this.selectedMusic.X = 450 + (this.musicID * 190);
                }
            }
        }

        public IEnumerable<IRenderable> GetObjects()
        {
            List<GameObject> list = new List<GameObject>();
            foreach (GUIElement item in this.gameData.GetGuiElements(1))
            {
                list.Add(item);
            }

            list.Add(this.playerName);
            list.Add(this.selectedDiff);
            list.Add(this.selectedMusic);

            return list;
        }

        public void KeyDown(string keyName)
        {
            if (keyName.Length == 1 && this.playerName.Text.Length <= 12)
            {
                this.playerName.Text += keyName[0];
            }
            else if (keyName.Equals("Backspace") && this.playerName.Text.Length > 0)
            {
                this.playerName.Text = this.playerName.Text.Substring(0, this.playerName.Text.Length - 1);
            }
        }
    }
}
