﻿using CSH.GameModel;
using CSH.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CSH.Logic
{
    public class GameLogic
    {
        private IModel model;
        private IGameData data;
        private IRepository repo;
        private MusicPlayer mp;

        private double musicDuration = 0;
        private int addCounter = 0;
        private int notesCount = 0;
        private bool isGameStarted;
        private bool isGameOver; // ha ismusic over, akkor is isgameover. ha isgameover, akkor ismusicover is
                                 // private bool isMusicPaused; 

        Stopwatch sw;

        private static Random rnd;
        private static int range;


        public GameLogic(IModel model, IGameData data, IRepository repo)
        {
            this.model = model;
            this.data = data;
            this.repo = repo;
            this.mp = new MusicPlayer();
            rnd = new Random();

            this.isGameStarted = false;
            this.isGameOver = false;
        }


        // Ez lehet nem is kell
        public void GameEnd()
        {
            this.isGameOver = true;
            
            this.repo.SaveHighscore(new Record(model.PlayerName, model.Score, model.Streak, model.Difficulty));
        }

        public void AddNotes()
        {
            notesCount = 40*(model.Difficulty+1);
            int range = 360; // pixel
            int minPosition;
            int maxPosition;
            int position;
            for (int i = 0; i < notesCount; i++)
            {
                maxPosition = -(i * range);
                minPosition = -((i + 1) * range);
                position = rnd.Next(minPosition, maxPosition);
                model.Notes1.Add(new Note(430, position, 90, 90, 4));
                position = rnd.Next(minPosition, maxPosition);
                model.Notes2.Add(new Note(570, position, 90, 90, 12));
                position = rnd.Next(minPosition, maxPosition);
                model.Notes3.Add(new Note(710, position, 90, 90, 16));
                position = rnd.Next(minPosition, maxPosition);
                model.Notes4.Add(new Note(850, position, 90, 90, 14));
            }
        }


        public void KeyDown(string key)
        {
            switch(key)
            {
                case "1": CheckKey1(); break;
                case "2": CheckKey2(); break;
                case "3": CheckKey3(); break;
                case "4": CheckKey4(); break;
            }
        }


        private void CheckKey1()
        {
            if (model.Notes1.Any(x => x.Y >= 560 && x.Y <= 720))
            {
                model.Score += model.Multiplier * (model.Difficulty + 1) * 2;
                model.Streak++;
                MultiplerSetter();
            }
            else
            {
                Miss();
                model.Streak = 0;
                model.Multiplier = 1;
            }
        }

        private void CheckKey2()
        {
            if (model.Notes2.Any(x => x.Y >= 560 && x.Y <= 720))
            {
                model.Score += model.Multiplier * (model.Difficulty + 1) * 2;
                model.Streak++;
                MultiplerSetter();
            }
            else
            {
                Miss();
                model.Streak = 0;
                model.Multiplier = 1;
            }
        }

        private void CheckKey3()
        {
            if (model.Notes3.Any(x => x.Y >= 560 && x.Y <= 720))
            {
                model.Score += model.Multiplier * (model.Difficulty + 1) * 2;
                model.Streak++;
                MultiplerSetter();
            }
            else
            {
                Miss();
                model.Streak = 0;
                model.Multiplier = 1;
            }
        }

        private void CheckKey4()
        {
            if (model.Notes4.Any(x => x.Y >= 560 && x.Y <= 720))
            {
                model.Score += model.Multiplier * (model.Difficulty + 1) * 2;
                model.Streak++;
                MultiplerSetter();
            }
            else
            {
                Miss();
                model.Streak = 0;
                model.Multiplier = 1;
            }
        }

        void MultiplerSetter()
        {
            if (model.Streak != 0 && model.Streak % 50 == 0) model.Multiplier++;
        }
        void Miss()
        {
            model.Score -= (model.Difficulty + 2) * 3;
        }

        void ScoreReducer()
        {
            if (model.Notes1.Any(x => x.Y > 720)||model.Notes2.Any(x => x.Y > 720) || model.Notes3.Any(x => x.Y > 720) || model.Notes4.Any(x => x.Y > 720))
            {
                //model.Notes1.
            }
        }

        public void Update()
        {
            if (isGameStarted == false) // --> Így futnak le egyszer a dolgok... Kell egy bool.
            {
                mp.MusicChooser(model.Difficulty);
                mp.Play();
                this.musicDuration = mp.GetMusicDuration();
                AddNotes();
                sw = new Stopwatch();
                sw.Start();
                isGameStarted = true;
            }
            else
            {
                MoveNotes();
                /*if (IsMusicOver(sw))
                {
                    this.isGameOver = true; --> Valamiért lefutott
                    repo.SaveHighscore(new Record(this.model.PlayerName, this.model.Score, this.model.Streak, this.model.Difficulty));
                }*/
            }
            if (isGameOver == true)
            {
                
            }
        }

        public bool IsMusicOver(Stopwatch sw)
        {
            if (sw.Elapsed.TotalSeconds >= /*this.musicDuration*/ 10) return true;
            else return false;
        }

        public void MoveNotes()
        {
            for (int i = 0; i < notesCount; i++)
            {
                if (model.Notes1[i] != null) model.Notes1[i].Y += model.Notes1[i].Dy + (model.Difficulty + 1) * 2;
                if (model.Notes2[i] != null) model.Notes2[i].Y += model.Notes2[i].Dy + (model.Difficulty + 1) * 2;
                if (model.Notes3[i] != null) model.Notes3[i].Y += model.Notes3[i].Dy + (model.Difficulty + 1) * 2;
                if (model.Notes4[i] != null) model.Notes4[i].Y += model.Notes4[i].Dy + (model.Difficulty + 1) * 2;
            }

        }

        public void MouseClick(double x, double y, ref int menuMode)
        {
            foreach (GUIElement item in this.data.GetGuiElements(3))
            {
                if (item.Collide(x,y,0,0))
                {
                    switch (item.Id)
                    {
                        case 66: 
                            isGameOver = true;
                            mp.Stop(); 
                            this.isGameOver = false;
                            this.isGameStarted = false;
                            repo.SaveHighscore(new Record(this.model.PlayerName, this.model.Score, this.model.Streak, this.model.Difficulty));
                            menuMode = 0;
                            break; //TESZT
                    }
                }
            }
        }

        public string DiffToString()
        {
            string ds;
            switch (this.model.Difficulty)
            {
                case 0:
                    ds = "EASY";
                    break;
                case 1:
                    ds = "MEDIUM";
                    break;
                case 2:
                    ds = "HARD";
                    break;
                default:
                    ds = "WTF";
                    break;
            }
            return ds;
        }

        public IEnumerable<IRenderable> GetObjects()
        {
            List<GameObject> list = new List<GameObject>();
            foreach (GUIElement item in this.data.GetGuiElements(3))
            {
                list.Add(item);
            }

            list.Add(new GUIElement(0, 1100, 75, 200, 30, 1, this.model.PlayerName));
            list.Add(new GUIElement(0, 1100, 100, 200, 30, 1, "DIFFICULTY: "+ DiffToString()));
            list.Add(new GUIElement(0, 1100, 650, 200, 30, 1, this.model.Music.Name));
            list.Add(new GUIElement(0, 180, 180, 200, 30, 1, this.model.Score.ToString()));
            list.Add(new GUIElement(0, 180, 400, 200, 30, 1, this.model.Streak.ToString()));
            list.Add(new GUIElement(0, 180, 620, 200, 30, 1, this.model.Multiplier.ToString()));

            for (int i = 0; i < notesCount; i++)
            {
                list.Add(model.Notes1[i]);
                list.Add(model.Notes2[i]);
                list.Add(model.Notes3[i]);
                list.Add(model.Notes4[i]);
            }

            if (this.isGameOver)
            {
                list.Add(new GameObject(640, 360, 400, 400, 27));
                // list.Add(new GUIElement(66, 640, 530, 100, 100, 14, "CLICK HERE TO EXIT"));
                repo.SaveHighscore(new Record(this.model.PlayerName, this.model.Score, this.model.Streak, this.model.Difficulty));
            }

            return list;
        }
    }
}
