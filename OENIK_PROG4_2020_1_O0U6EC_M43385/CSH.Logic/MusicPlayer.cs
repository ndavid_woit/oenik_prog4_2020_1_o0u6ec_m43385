﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Logic
{
    public class MusicPlayer
    {
        IList<string> musicList;
        IWavePlayer waveout;
        MediaFoundationReader musicReader;

        public MusicPlayer()
        {
            waveout = new WaveOutEvent();

            musicList = new List<string>();
            foreach (string file in Directory.EnumerateFiles("music")) musicList.Add(file);
        }

        public void MusicChooser(int musicidx)
        {
            //this.waveout.Dispose();
            //this.waveout = new WaveOutEvent();
            this.musicReader = new MediaFoundationReader(musicList[musicidx]);
            this.waveout.Init(musicReader);
        }

        public double GetMusicDuration()
        {
            return this.musicReader.TotalTime.TotalSeconds;
        }

        public void Play()
        {
            waveout.Play();
        }

        public void Stop()
        {
            waveout.Stop();
        }
    }
}
