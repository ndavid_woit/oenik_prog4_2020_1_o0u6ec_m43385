﻿using CSH.GameModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Logic
{
    public class MainMenuLogic
    {
        private IGameData gameData;

        public MainMenuLogic(IGameData gameData)
        {
            this.gameData = gameData;


        }

        public void MouseClick(double x, double y, ref int menuMode)
        {
            foreach (GUIElement item in this.gameData.GetGuiElements(0))
            {
                if (item.Collide(x, y, 0, 0))
                {
                    switch (item.Id)
                    {
                        case 1: menuMode = 1; break;
                        case 2: menuMode = 2; break;
                        case 3: Environment.Exit(0); break;
                    }
                }
            }
        }

        public IEnumerable<IRenderable> GetObjects()
        {
            List<GameObject> list = new List<GameObject>();
            foreach (GUIElement item in this.gameData.GetGuiElements(0))
            {
                list.Add(item);
            }

            return list;
        }
    }
}
