﻿using CSH.GameModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Logic
{
    public interface ILogic
    {
        IEnumerable<IRenderable> GetObjects();

        void Update();

        void MouseClick(double x, double y);

        void KeyDown(string keyName);

    }
}
