﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CSH.GameModel
{
    public class Note : GameObject
    {
        public double Dy { get; set; }

        public Note(double x, double y, double width, double height, int textureId) : base(x, y, width, height, textureId)
        {
            this.Dy = 5;
        }

    }
}
