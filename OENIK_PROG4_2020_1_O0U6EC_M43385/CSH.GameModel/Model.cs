﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CSH.GameModel
{
    public class Model : IModel
    {
        public Model()
        {
            this.PlayerName = string.Empty;
            this.Difficulty = 1;
            this.Score = 0; //score = sikeresen leütött gomb * multiplier
            this.Streak = 0; 
            this.Multiplier = 1; //10-es streak --> 2-es multiplier. 50-es streak --> 3-as multiplier
            this.Notes1 = new List<Note>();
            this.Notes2 = new List<Note>();
            this.Notes3 = new List<Note>();
            this.Notes4 = new List<Note>();
            this.Music = null;
        }
        public string PlayerName { get; set; }
        public int Difficulty { get; set; }
        public int Score { get; set; }
        public int Streak { get; set; }
        public int Multiplier { get; set; }
        public List<Note> Notes1 { get; set; }
        public List<Note> Notes2 { get; set; }
        public List<Note> Notes3 { get; set; }
        public List<Note> Notes4 { get; set; }
        public Music Music { get; set; }


        public void LoadNewGame(IGameData data, string playerName, int musicID, int difficulty, int score)
        {
            this.PlayerName = playerName;
            this.Music = data.GetMusic(musicID);
            this.Difficulty = difficulty;
            this.Score = score;
            this.Notes1 = new List<Note>();
            this.Notes2 = new List<Note>();
            this.Notes3 = new List<Note>();
            this.Notes4 = new List<Note>();
        }


    }
}
