﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CSH.GameModel
{
    public static class Config
    {
        public static double Width = 1200;
        public static double Height = 720;

        public static Brush Note1Color = Brushes.Orange;
        public static Brush Note2Color = Brushes.Green;
        public static Brush Note3Color = Brushes.Purple;
        public static Brush Note4Color = Brushes.LightGoldenrodYellow;
    }
}
