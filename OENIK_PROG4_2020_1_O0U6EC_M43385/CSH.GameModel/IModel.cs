﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CSH.GameModel
{
    public interface IModel
    {
        string PlayerName { get; set; }
        int Difficulty { get; set; }
        int Score { get; set; }
        int Streak { get; set; }
        int Multiplier { get; set; }
        List<Note> Notes1 { get; set; }
        List<Note> Notes2 { get; set; }
        List<Note> Notes3 { get; set; }
        List<Note> Notes4 { get; set; }
        Music Music { get; set; }
        void LoadNewGame(IGameData data, string playerName, int musicID, int difficulty, int score);
    }
}
