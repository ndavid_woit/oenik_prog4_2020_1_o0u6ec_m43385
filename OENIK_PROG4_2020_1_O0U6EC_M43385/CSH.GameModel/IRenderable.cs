﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.GameModel
{
    public interface IRenderable
    {        
        double X { get; set; }
        double Y { get; set; } 
        double Width { get; set; }  
        double Height { get; set; }
        string Text { get; set; }
        int TextureId { get; set; }
        bool IsVisible { get; set; }
    }
}
