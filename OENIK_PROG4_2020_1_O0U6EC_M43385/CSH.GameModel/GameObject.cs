﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.GameModel
{
    public class GameObject : IRenderable
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        //public double Rotation { get; set; }
        public string Text { get; set; }
        public int TextureId { get; set; }
        public bool IsVisible { get; set; }

        public GameObject()
        {
            this.X = 0;
            this.Y = 0;
            this.Width = 0;
            this.Height = 0;
            //this.Rotation = 0;
            this.Text = string.Empty;
            this.TextureId = 0;
            this.IsVisible = true;
        }
        public GameObject(double x, double y, double width, double height, int textureId)
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
            //this.Rotation = 0;
            this.Text = string.Empty;
            this.TextureId = textureId;
            this.IsVisible = true;
        }
        public GameObject(GameObject other)
        {
            this.X = other.X;
            this.Y = other.Y;
            this.Width = other.Width;
            this.Height = other.Height;
            //this.Rotation = other.Rotation;
            this.Text = other.Text;
            this.TextureId = other.TextureId;
            this.IsVisible = other.IsVisible;
        }

        public bool Collide(double x, double y, double w, double h)
        {
            return (this.X + (this.Width / 2)) > (x - (w / 2))
                && (this.X - (this.Width / 2)) < (x + (w / 2))
                && (this.Y + (this.Height / 2)) > (y - (h / 2))
                && (this.Y - (this.Height / 2)) < (y + (h / 2));
        }
    }
}
