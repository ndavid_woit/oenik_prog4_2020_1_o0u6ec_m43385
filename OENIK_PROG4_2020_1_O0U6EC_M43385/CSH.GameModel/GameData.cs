﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.GameModel
{
    public class GameData : IGameData
    {
        private List<List<GUIElement>> guiElements;
        private List<Music> musics;
        public Difficulty[] Difficulties { get; set; }

        public void GameDataLoad() //konstruktor-szerű
        {
            this.Difficulties = new Difficulty[3];
            Difficulties[0] = (new Difficulty(0, "EASY", 100, "Bon Jovi - Livin' On A Prayer", 50));
            Difficulties[1] = (new Difficulty(1, "MEDIUM", 200, "Tankcsapda - Fekszem a foldon", 100));
            Difficulties[2] = (new Difficulty(2, "HARD", 500, "DragonForce - Through The Fire And Flames", 9999));

            this.musics = new List<Music>
            {
                new Music(0,Difficulties[0].MusicName,1),
                new Music(1,Difficulties[1].MusicName,2),
                new Music(2,Difficulties[2].MusicName,3)
            };

            // Menu gui element definitions
            this.guiElements = new List<List<GUIElement>>();

            List<GUIElement> mainMenu = new List<GUIElement>
            {
                new GUIElement(0, 640, 360, 1280, 720, 8, string.Empty), // Háttérkép
                new GUIElement(1, 300, 350, 575, 75, 10, string.Empty), //new game
                new GUIElement(2, 338, 440, 575, 75, 6, string.Empty), //highscores
                new GUIElement(3, 183, 530, 575, 75, 2, string.Empty) //exit
            };
            this.guiElements.Add(mainMenu);

            List<GUIElement> newGameMenu = new List<GUIElement>
            {
                new GUIElement(0, 640, 360, 1280, 720, 9, string.Empty), //háttérkép
                new GUIElement(0, 500, 540, 80, 50, 1, "NAME:"),
                new GUIElement(1, 120, 630, 100, 50, 0, string.Empty), //backbutton
                new GUIElement(2, 640, 630, 150, 50, 13, string.Empty), //playbutton
                new GUIElement(0, 250, 410, 160, 90, 1, "DIFFICULTY:"),
                new GUIElement(3, 450, 410, 160, 90, 1, this.Difficulties[0].DifficultyName),
                new GUIElement(4, 640, 410, 160, 90, 1, this.Difficulties[1].DifficultyName),
                new GUIElement(5, 830, 410, 160, 90, 1, this.Difficulties[2].DifficultyName),
                //new GUIElement(0, 250, 230, 160, 90, 1, "MUSIC:"),
                //new GUIElement(6, 450, 230, 160, 90, 4, string.Empty),
                //new GUIElement(7, 640, 230, 160, 90, 4, string.Empty),
                //new GUIElement(8, 830, 230, 160, 90, 4, string.Empty),
                //new GUIElement(0, 450, 300, 160, 90, 1, musics[0].Name),
                //new GUIElement(0, 640, 300, 160, 90, 1, musics[1].Name),
                //new GUIElement(0, 830, 300, 160, 90, 1, musics[2].Name)
            };
            this.guiElements.Add(newGameMenu);

            List<GUIElement> highScoreMenu = new List<GUIElement>
            {
                new GUIElement(3, 640, 360, 1280, 720, 5, string.Empty),
                new GUIElement(0, 120, 630, 100, 50, 0, string.Empty)
            };
            this.guiElements.Add(highScoreMenu);

            List<GUIElement> gameMenu = new List<GUIElement>
            {
                new GUIElement(0, 640, 360, 1280, 720, 3, string.Empty),
                new GUIElement(66, 1100, 360,300,100,14,"CLICK HERE TO EXIT")
            };
            this.guiElements.Add(gameMenu);
        }

        public List<GUIElement> GetGuiElements(int id)
        {
            if (this.guiElements.Count > id)
            {
                return this.guiElements[id];
            }

            return null;
        }

        public Music GetMusic(int id)
        {
            foreach (Music item in this.musics)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }

    }
}
