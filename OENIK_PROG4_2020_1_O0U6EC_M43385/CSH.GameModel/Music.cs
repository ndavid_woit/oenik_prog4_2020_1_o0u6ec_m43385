﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CSH.GameModel
{
    public class Music : MediaPlayer
    {
        public Music(int id, string name, int musicId)
        {
            this.Id = id;
            this.Name = name;
            this.MusicId = musicId;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public int MusicId { get; set; }
    }
}
