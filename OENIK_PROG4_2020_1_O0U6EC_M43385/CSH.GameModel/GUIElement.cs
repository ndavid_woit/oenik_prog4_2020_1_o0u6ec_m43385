﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.GameModel
{
    public class GUIElement : GameObject
    {
        public int Id { get; set; }

        public GUIElement(int id, double x, double y, double width, double height, int textureId, string text) : base(x, y, width, height, textureId)
        {
            this.Id = id;
            this.Text = text;
        }

    }
}
