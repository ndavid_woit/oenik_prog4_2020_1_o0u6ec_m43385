﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.GameModel
{
    public class Difficulty
    {
        public int DiffID { get; set; }
        public string DifficultyName { get; set; }
        public int NoteAmount { get; set; }
        public string MusicName { get; set; }
        public int RequiredPoints { get; set; }
        //public Music Music { get; set; }

        public Difficulty(int diffId, string dname, int amount, string mname, int req/*, Music music*/)
        {
            this.DiffID = diffId;
            this.DifficultyName = dname;
            this.NoteAmount = amount;
            this.MusicName = mname;
            this.RequiredPoints = req;
            //this.Music = music;
        }
    }
}
