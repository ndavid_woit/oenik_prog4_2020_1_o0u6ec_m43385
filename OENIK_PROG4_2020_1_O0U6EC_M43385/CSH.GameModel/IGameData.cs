﻿using System.Collections.Generic;

namespace CSH.GameModel
{
    public interface IGameData
    {
        Difficulty[] Difficulties { get; set; }
        void GameDataLoad();
        List<GUIElement> GetGuiElements(int id);
        Music GetMusic(int id);
    }
}