﻿using CSH.GameModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OENIK_PROG4_2020_1_O0U6EC_M43385
{
    public class GameRenderer
    {
        private Typeface font;
        private ImageBrush[] images;

        public GameRenderer()
        {
            this.font = new Typeface("arial");
            this.images = new ImageBrush[50];
            int idx = 0;
            foreach (string file in Directory.EnumerateFiles("images"))
            {
                this.images[idx] = new ImageBrush(new BitmapImage(new Uri(file, UriKind.Relative)));
                idx++;
            }
        }

        public void Render(IEnumerable<IRenderable> objects, DrawingContext ctx)
        {
            foreach (IRenderable item in objects)
            {
                if (item != null)
                {
                    if (item.IsVisible)
                    {
                        Geometry g = new RectangleGeometry(new Rect(-(item.Width / 2), -(item.Height / 2), item.Width, item.Height));
                        ctx.PushTransform(new TranslateTransform(item.X, item.Y));
                        ctx.DrawGeometry(this.images[item.TextureId], null, g.GetFlattenedPathGeometry());
                        ctx.Pop();
                        if (item.Text != null && item.Text != string.Empty)
                        {
                            FormattedText text = new FormattedText(item.Text,
                                CultureInfo.CurrentCulture,
                                FlowDirection.LeftToRight,
                                this.font,
                                17,
                                Brushes.OrangeRed);
                            ctx.DrawText(text, new Point(item.X - (4 * item.Text.Length), item.Y - 8));
                        }
                    }
                }
            }
        }
    }
}
