﻿using CSH.GameModel;
using CSH.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using Timer = System.Timers.Timer;
using System.Windows.Input;
using System.Windows.Media;
using CSH.Repository;

namespace OENIK_PROG4_2020_1_O0U6EC_M43385
{
    internal class GameControl : FrameworkElement
    {
        const int UpdateInterval = 60;
        Timer updateTimer;

        public ILogic logic;
        GameRenderer renderer;

        public GameControl()
        {
            this.Loaded += Loader;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.Render(this.logic.GetObjects(), drawingContext);
            }
        }

        void Loader(object sender, RoutedEventArgs e)
        {
            GameData gd = new GameData();
            gd.GameDataLoad();
            this.logic = new Logic(gd, new Model(), new XmlRepo());
            this.renderer = new GameRenderer();
            this.updateTimer = new Timer();
            this.updateTimer.Elapsed += new ElapsedEventHandler(this.Update);
            this.updateTimer.Interval = UpdateInterval;
            this.updateTimer.AutoReset = true;
            this.updateTimer.Enabled = true;
            Window win = Window.GetWindow(this);
            win.MouseLeftButtonDown += this.Click;
            // win.MouseMove += this.MouseMove;
            win.KeyDown += this.KeyDown;
        }

        void Click(object sender, MouseButtonEventArgs e)
        {
            logic.MouseClick(e.GetPosition((IInputElement)sender).X, e.GetPosition((IInputElement)sender).Y);
        }

        /*new void MouseMove(object sender, MouseEventArgs e)
        {
            logic.Mou(e.GetPosition((IInputElement)sender).X, e.GetPosition((IInputElement)sender).Y);
        }*/

        new void KeyDown(object sender, KeyEventArgs e)
        {
            logic.KeyDown(new KeyConverter().ConvertToString(e.Key));
        }

        void Update(object sender, ElapsedEventArgs e)
        {
            logic.Update();
            try { this.Dispatcher.Invoke(() => this.InvalidateVisual()); } //képfrissítés kényszerítése
            catch (Exception) { }
        }

    }
}
