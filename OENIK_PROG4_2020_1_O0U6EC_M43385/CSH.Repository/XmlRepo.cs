﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CSH.Repository
{
    public class XmlRepo : IRepository
    {
        static List<Record> savedGames = new List<Record>();

        /*Model nem lehet a Repoban. A Repo nem adhat vissza modelt. De erre nem is lesz szükség szerintem.
         * public Model GameLoader(string playerName)
        {
            Model m = new Model();

            savedGames = LoadHighscores();
            foreach (var item in savedGames)
            {
                if (item.PlayerName.Equals(playerName))
                {
                    m.Difficulty = item.DiffID;
                    m.Score = item.Highscore;
                }
            }

            return m;
        }*/

        public void SaveHighscore(Record rec)
        {
            var found = savedGames?.FirstOrDefault(r => r.PlayerName == rec.PlayerName); // a ?. azért kell, mert még lehet, hogy nem létezik a lista. (Első alkalommal)
            if (found != null) // Ha benne van, akkor a Score.ját kell növelni
            {
                found = rec;
            }
            else
            {
                savedGames?.Add(rec);
            }

            XmlCreator(savedGames);
        }

        public List<Record> LoadHighscores()
        {
            savedGames = XmlReader();

            return savedGames;
        }

        private List<Record> XmlReader()
        {
            List<Record> readedSaves = new List<Record>();

            if (!File.Exists("savedgames.xml")) return null;

            try
            {
                XDocument xml = XDocument.Load("savedgames.xml");
                foreach (var item in xml.Descendants("record"))
                {
                    Record newrec = new Record();
                    newrec.PlayerName = item.Attribute("Pname").Value;
                    newrec.MaxStreak = int.Parse(item.Attribute("MaxStreak").Value);
                    newrec.Highscore = int.Parse(item.Attribute("Highscore").Value);
                    newrec.Difficulty =item.Attribute("Diff").Value;

                    readedSaves.Add(newrec);
                }
            }
            catch (Exception)
            {
                return null;
            }

            return readedSaves;
        }

        private void XmlCreator(List<Record> savedGames)
        {
            XDocument xml;
            XElement saved;

            if (File.Exists("savedgames.xml"))
            {
                xml = XDocument.Load("savedgames.xml");
                saved = xml.Element("savedgames");
            }
            else
            {
                xml = new XDocument();
                saved = new XElement("savedgames");
                xml.Add(saved);
            }

            if (savedGames != null)
            {
                foreach (var item in savedGames)
                {
                    XElement rec = new XElement("record");

                    rec.Add(new XAttribute("Pname", item.PlayerName));
                    rec.Add(new XAttribute("Diff", item.Difficulty));
                    rec.Add(new XAttribute("MaxStreak", item.MaxStreak));
                    rec.Add(new XAttribute("Highscore", item.Highscore));

                    saved.Add(rec);
                }
            }


            xml.Save("savedgames.xml");
        }
    }
}
