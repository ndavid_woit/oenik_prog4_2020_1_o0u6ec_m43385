﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Repository
{
    public interface IRepository
    {
        void SaveHighscore(Record rec);

        List<Record> LoadHighscores();
    }
}
