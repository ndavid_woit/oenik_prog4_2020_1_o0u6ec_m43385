﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSH.Repository
{
    public class Record
    {
        public string PlayerName { get; set; }
        public int Highscore { get; set; }
        public int MaxStreak { get; set; }
        public string Difficulty { get; set; }
        public int DiffID { get; set; }

        public Record()
        {

        }

        public Record(string name, int hs, int ms, int diffID)
        {
            this.PlayerName = name;
            this.Highscore = hs;
            this.MaxStreak = ms;
            this.DiffID = diffID;

            switch (this.DiffID)
            {
                case 0:
                    this.Difficulty = "EASY";
                    break;
                case 1:
                    this.Difficulty = "MEDIUM";
                    break;
                case 2:
                    this.Difficulty = "HARD";
                    break;

                default:
                    this.Difficulty = "EASY";
                    break;
            }
        }
    }
}
